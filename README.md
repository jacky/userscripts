# userscripts

Collection of my Greasemonkey userscripts.

## Install

1. Install [Greasemonkey](https://www.greasespot.net/) or any compatible
userscript manager.
2. Install the userscripts you want.
