// ==UserScript==
// @name        Dynasty Flip Title
// @version     1.00
// @description Change tab title on dynasty-scans for easier identification
// @match       *://dynasty-scans.com/*
// @grant       none
// @run-at      document-end
// ==/UserScript==

document.title = document.title.split(" » ").reverse().join(" | ");
