// ==UserScript==
// @name               Nitter Redirect
// @version            1.00
// @description        Redirect twitter to nitter
// @include            *://twitter.com/*
// @include            *://*.twitter.com/*
// @run-at             document-start
// @grant              none
// ==/UserScript==
 
(function() {
	// replace with your nitter instance of choice
	const nitter = "https://nitter.privacydev.net";
	window.location.replace(nitter + window.location.pathname +
							window.location.search + window.location.hash);
})();
