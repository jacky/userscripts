// ==UserScript==
// @name        Wikipedia Skin
// @version     1.00
// @description Set wikipedia skin
// @match       *://*.wikipedia.org/*
// @exclude     *://*.wikipedia.org/*?useskin=*
// @exclude     *://*.wikipedia.org/*&useskin=*
// @run-at      document-start
// @grant       none
// ==/UserScript==

(function() {
	// valid skins: vector-2022, vector, monobook, timeless, minerva, modern, cologneblue
	const skin = "vector";
	let delim = window.location.search.length == 0 ? "?" : "&";
	let new_url = window.location.protocol + '//' + window.location.host +
		window.location.pathname + window.location.search + delim +
		"useskin=" + skin + window.location.hash;
	window.location.replace(new_url);
})();
