// ==UserScript==
// @name               Invidious Redirect
// @version            1.00
// @description        Redirect youtube to invidious
// @include            *://youtube.com/*
// @include            *://*.youtube.com/*
// @run-at             document-start
// @grant              none
// ==/UserScript==
 
(function() {
	// replace with your invidious instance of choice
	const invid = "https://super8.absturztau.be";
	window.location.replace(invid + window.location.pathname +
							window.location.search + window.location.hash);
})();
