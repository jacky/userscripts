// ==UserScript==
// @name               Fandom NoVideo
// @version            1.00
// @description        Don't show the "featured video" on fandom.com
// @include            *://fandom.com/*
// @include            *://*.fandom.com/*
// @run-at             document-end
// @grant              none
// ==/UserScript==

document.querySelector('[data-name="featured-video-mapped-to-wiki"]').remove();
document.body.classList.add('no-featured-video');
