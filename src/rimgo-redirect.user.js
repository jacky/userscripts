// ==UserScript==
// @name               Rimgo Redirect
// @version            1.00
// @description        Redirect imgur to rimgo
// @include            *://imgur.com/*
// @include            *://*.imgur.com/*
// @run-at             document-start
// @grant              none
// ==/UserScript==
 
(function() {
	// replace with your rimgo instance of choice
	const rimgo = "https://imgur.artemislena.eu";
	window.location.replace(rimgo + window.location.pathname +
							window.location.search + window.location.hash);
})();
